x2go.backends.control package
=============================

Submodules
----------

.. toctree::

   x2go.backends.control.plain

Module contents
---------------

.. automodule:: x2go.backends.control
    :members:
    :undoc-members:
    :show-inheritance:
