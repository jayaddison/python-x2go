# -*- coding: utf-8 -*-

# Copyright (C) 2010-2023 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# Python X2Go is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Python X2Go is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

__NAME__    = 'python-x2go'
__VERSION__ = '0.6.1.4'
__AUTHOR__  = 'Mike Gabriel'

__package__ = 'x2go'
__name__    = 'x2go'

import os
from .defaults import X2GOCLIENT_OS

if X2GOCLIENT_OS != 'Windows':
    # in Debian unstable, the ares resolver is broken, see
    # https://bugs.debian.org/908144
    #os.environ["GEVENT_RESOLVER"] = "ares"

    # we could use the dnspython resolver...
    #os.environ["GEVENT_RESOLVER"] = "dnspython"

    # however, this is unsupported in Debian 9, so we do nothing here...
    pass

from gevent import monkey
monkey.patch_all()

from . import utils

from .client import X2GoClient
from .backends.profiles.file import X2GoSessionProfiles
from .backends.printing.file import X2GoClientPrinting
from .backends.settings.file import X2GoClientSettings
from .session import X2GoSession
from .sshproxy import X2GoSSHProxy
from .x2go_exceptions import *
from .log import *

from .cleanup import x2go_cleanup

from .defaults import CURRENT_LOCAL_USER
from .defaults import LOCAL_HOME
from .defaults import X2GO_CLIENT_ROOTDIR
from .defaults import X2GO_SESSIONS_ROOTDIR
from .defaults import X2GO_SSH_ROOTDIR
from .defaults import BACKENDS

if X2GOCLIENT_OS == 'Windows':
    from .xserver import X2GoClientXConfig, X2GoXServer

# compat section
X2goClient = X2GoClient
X2goSessionProfiles = X2GoSessionProfiles
X2goClientPrinting = X2GoClientPrinting
X2goClientSettings = X2GoClientSettings
X2goSession = X2GoSession
X2goSSHProxy = X2GoSSHProxy

if X2GOCLIENT_OS == 'Windows':
    X2goClientXConfig = X2GoClientXConfig
    X2goXServer = X2GoXServer
