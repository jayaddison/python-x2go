#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2010-2023 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# Python X2Go is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Python X2Go is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

###
### short example for python-x2go usage
###

# import x2go before you import other thread based modules (e.g. paramiko)
import x2go

import sys
import getpass
import gevent

# modify to your needs...
server   = "server.mydomain.tld"
port     =  22
username = "foo"

password = getpass.getpass()

cli = x2go.X2GoClient(use_cache=False, loglevel=x2go.log.loglevel_DEBUG)
s_uuid = cli.register_session(server, port=port, username=username, geometry="800x600", add_to_known_hosts=True)
cli.connect_session(s_uuid, password=password)

# the next lines will resume the first session in the avail_sessions dictionary
avail_sessions = cli.list_sessions(s_uuid)
if avail_sessions:
    session_info = list(avail_sessions.values())[0]
    sys.stdout.write("Trying to resume session %s\n" % (session_info.name,))
    cli.resume_session(s_uuid, session_name=session_info.name)

    try:
        while cli.session_ok(s_uuid):
            gevent.sleep(2)
    except KeyboardInterrupt:
        pass

    cli.terminate_session(s_uuid)

else:
    print("No sessions to be resumed.")


